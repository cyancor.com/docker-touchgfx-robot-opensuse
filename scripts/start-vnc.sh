#!/bin/bash

echo "Starting VNC server..."
export DISPLAY=:1

if [ "$RESOLUTION" != "" ]; then
    dbus-launch vncserver -geometry $RESOLUTION $DISPLAY &
else
    dbus-launch vncserver $DISPLAY &
fi

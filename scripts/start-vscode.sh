 #!/bin/bash

if [ "$VSCODE_PASSWORD" == "" ]; then
    Auth="none"
else
    Auth="password"
fi

PASSWORD="$VSCODE_PASSWORD" code-server \
    --bind-addr 0.0.0.0:4242 \
    --user-data-dir /vscode/data \
    --extensions-dir /vscode/extensions \
    --disable-telemetry \
    --auth "$Auth" \
    "$@"

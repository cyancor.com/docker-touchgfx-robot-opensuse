FROM opensuse/leap:15.2
LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

EXPOSE 5901
EXPOSE 6080

ENV SHELL="/bin/bash" \
    DEBIAN_FRONTEND="noninteractive" \
    PATH="$PATH:/gcc-arm-none-eabi/bin:/usr/games" \
    TERM="xterm" \
    JYTHON_VERSION="2.7.2" \
    GNU_ARM_EMBEDDED_URL="https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-rm/9-2020q2/gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2"

COPY scripts/* /
COPY configurations/xfce4-panel.xml /root/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml
COPY scripts/start-vscode.sh /start-vscode.sh

RUN zypper addrepo https://download.opensuse.org/repositories/windows:mingw:win32/openSUSE_Leap_15.2/windows:mingw:win32.repo \
    && zypper addrepo https://download.opensuse.org/repositories/openSUSE:Leap:15.2:Update/standard/openSUSE:Leap:15.2:Update.repo \
    && zypper addrepo https://download.opensuse.org/repositories/X11:Utilities/openSUSE_Leap_15.2/X11:Utilities.repo \
    && zypper addrepo -cfp 90 'https://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Leap_15.2/' packman \
    && zypper --gpg-auto-import-keys --non-interactive refresh \
    && zypper --non-interactive dist-upgrade --from packman --allow-vendor-change \
    && zypper --non-interactive install \
        git \
        curl \
        wget \
        htop \
        telnet \
        iproute2 \
        lsof \
        cowsay \
        netcat \
        vlc \
        zip \
        tar \
        bzip2 \
        unzip \
        gzip \
        nano \
        which \
        xz \
        ImageMagick \
        patterns-openSUSE-xfce \
        tightvnc \
        libxml2 \
        iputils \
        xeyes \
        libicu \
        NetworkManager \
        # Build tools
        make \
        cmake \
        ccache \
        mingw32-cross-gcc-c++ \
        gcc-c++ \
        gdb \
        ruby2.5-devel \
        libpng12-0 \
        libncurses5 \
        mono-complete \
        java-11-openjdk \
        python3 \
        python3-pip \
    && zypper --non-interactive install --from packman \
        ffmpeg \
        gstreamer-plugins-bad \
        gstreamer-plugins-libav \
        gstreamer-plugins-ugly \
        libavcodec58 \
        libavdevice58 \
        libavfilter7 \
        libavformat58 \
        libavresample4 \
        libavutil56 \
        vlc-codecs \
    # Ruby Gems
    && printf "\n\033[0;36mInstalling Ruby Gems...\033[0m\n" \
    && gem install nokogiri -v "1.6.1" --no-ri --no-rdoc \
    && gem install roo --no-ri --no-rdoc \
    # Python and Pip
    && printf "\n\033[0;36mInstalling Python and Pip...\033[0m\n" \
    && printf '#!/bin/bash\npython3 "$@"\n' > /usr/bin/python \
    && chmod 0777 /usr/bin/python \
    && pip install --upgrade pip \
    # GCC ARM
    && printf "\n\033[0;36mInstalling ARM GCC...\033[0m\n" \
    && curl -o /gcc-arm-none-eabi.tar.bz2 "${GNU_ARM_EMBEDDED_URL}" \
    && tar xjf /gcc-arm-none-eabi.tar.bz2 \
    && rm /gcc-arm-none-eabi.tar.bz2 \
    && mv /gcc-arm-none-eabi-* /gcc-arm-none-eabi \
    # .NET
    && printf "\n\033[0;36mInstalling .NET...\033[0m\n" \
    && rpm --import https://packages.microsoft.com/keys/microsoft.asc \
    && wget https://packages.microsoft.com/config/opensuse/15/prod.repo \
    && mv prod.repo /etc/zypp/repos.d/microsoft-prod.repo \
    && chown root:root /etc/zypp/repos.d/microsoft-prod.repo \
    && zypper --non-interactive install dotnet-sdk-5.0 \
    # Robot Framework
    && printf "\n\033[0;36mInstalling Robot Framework...\033[0m\n" \
    && wget "https://repo1.maven.org/maven2/org/python/jython-installer/${JYTHON_VERSION}/jython-installer-${JYTHON_VERSION}.jar" \
    && java -jar "jython-installer-${JYTHON_VERSION}.jar" -s -d /opt/jython \
    && python3 -m pip install robotframework-python3 \
    && python3 -m pip install \
        robotframework-requests \
        robotframework-websocketclient \
        robotframework-rammbock-py3 \
    # Visual Studio Code
    && printf "\n\033[0;36mInstalling Visual Studio Code...\033[0m\n" \
    && mkdir -p /vscode/data /vscode/extensions \
    && chmod -R 0777 /vscode \
    && curl -fsSL https://code-server.dev/install.sh | sh \
    && chmod 0777 /start-vscode.sh \
    # VNC
    && printf "\n\033[0;36mPreparing VNC...\033[0m\n" \
    && mkdir ~/.vnc \
    && echo "12345" | vncpasswd -f >> ~/.vnc/passwd \
    && chmod 600 ~/.vnc/passwd \
    && printf '#!/bin/sh\nunset SESSION_MANAGER\nunset DBUS_SESSION_BUS_ADDRESS\ndbus-launch startxfce4 &\n' > ~/.vnc/xstartup \
    && chmod +x ~/.vnc/xstartup \
    && touch /root/.Xauthority \
    # noVNC
    && printf "\n\033[0;36mInstalling noVNC...\033[0m\n" \
    && cd /opt \
    && git clone https://github.com/kanaka/noVNC \
    && cd /opt/noVNC/utils \
    && git clone https://github.com/novnc/websockify \
    && cp /opt/noVNC/vnc_lite.html /opt/noVNC/index.html \
    && sed -i -e 's/prompt(\"Password Required:\")/\"12345\"/g' /opt/noVNC/index.html \
    # Permissions
    && printf "\n\033[0;36mUpdating permissions...\033[0m\n" \
    && chmod +x /*.sh \
    # Info files
    && printf "\n\033[0;36mCreating info files...\033[0m\n" \
    && mkdir -p /info \
    && touch /info/IsDocker \
    && cat /etc/os-release | grep VERSION_ID | cut -d "\"" -f 2 > /info/opensuse \
    && make -v | head -n 1 | cut -d " " -f 3 > /info/make \
    && arm-none-eabi-gcc -v 2>&1 | grep "gcc version" | cut -d " " -f 3-4 | tr ' ' '-' > /info/arm-gcc \
    && code-server -v | tail -1 | cut -d " " -f 1 > /info/vscode \
    && python -m robot --version | cut -d " " -f 3 > /info/robot \
    # Cleanup
    && printf "\n\033[0;36mCleaning up...\033[0m\n" \
    && zypper clean \
    && rm /tmp/* -rf \
    # Done
    && printf "\n\033[0;36mDone.\033[0m\n"

CMD [ "/run.sh" ]